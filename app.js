const express = require("express");
const app = express();
const port = 3000;
const logger = require("./middleware/logger.js")
const { Article } = require("./models");
let users = require("./db/users.json")

app.set('view engine', 'ejs')
app.use(express.static("./public"));
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(logger)

app.get('/', (req, res) => res.status(200).render('index'))
app.get('/gamepage', (req, res) => res.status(200).render('gamepage'))


// Login 
app.get('/login', (req, res) => res.status(200).render('login'))
app.post('/login', (req, res) => {
    const { username, password } = req.body;
    const user = users.find((i) => {
        i.username === username && i.password === password;
      });
    if(user !== false){

        res.redirect('/dashboard')
    }
    else{
        res.redirect('/login')
    }
})

//dashboard 
app.get('/dashboard', (req, res) => {
    Article.findAll()
    .then(users => {
        res.render('dashboard', {users})
    })
})

//create
app.get('/create', (req, res) => res.status(200).render('create'))
app.post("/create", (req, res) => {
    Article
      .create({
        username: req.body.username,
        password: req.body.password,
      })
      .then(() => res.redirect("/"));
  });

//update
app.get("/update/:id", (req, res) => {
  Article
    .findOne({ where: { id: req.params.id } })
    .then((user) => res.render("update", { user }));
});

app.post("/update/:id", (req, res) => {
  Article
    .update(
      {
        username: req.body.username,
        password: req.body.password,
      },
      {
        where: { id: req.params.id },
      }
    )
    .then(() => res.redirect("/dashboard"));
});

//error handling

app.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({
      status: 'fail',
      errors: err.message
  })
  next()
})

app.use((req, res, next) => {
  res.status(404).json({
      status: 'fail',
      errors: 'Page not found'
  })
  next()
})


app.listen(port, () => console.log(`Server listening on port: ${port}`))